package com.schoolofnet.WebAppThymeleaf.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.schoolofnet.WebAppThymeleaf.model.City;

public interface CityRepository extends JpaRepository<City, Long> {
	
}
