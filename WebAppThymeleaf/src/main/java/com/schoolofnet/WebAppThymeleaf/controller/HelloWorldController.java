package com.schoolofnet.WebAppThymeleaf.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HelloWorldController {

	@GetMapping
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/hello");
		
		mv.addObject("name", "Edgar");
		
		return mv;
	}
	
	@GetMapping("/new")
	public String newPath(Model model) {
		List<String> list = new ArrayList<String>();
		
		list.add("Edgar");		
		list.add("Caio");		
		list.add("Marieli");		
		list.add("Marina");
		list.add("Maria Teresa");
		
		model.addAttribute("name", "Edgar");
		model.addAttribute("list", list);
		return "new";
	}
}
